<?xml version="1.0" encoding="UTF-8"?>
<ilog.rules.studio.model.dt:DecisionTable xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:ilog.rules.studio.model.dt="http://ilog.rules.studio/model/dt.ecore">
  <eAnnotations source="ilog.rules.custom_properties">
    <details key="API" value="BP"/>
    <details key="Business Group" value="Compliance Validation Rules"/>
    <details key="Business User" value="Jeremy Colvin"/>
    <details key="Prod Date" value="04/30/2017"/>
    <details key="Rule ID" value="R306"/>
  </eAnnotations>
  <name>UBP_PREPAY_ReceiverNameNonEmptyChecks</name>
  <uuid>5d8ac252-9ff3-4742-8487-636cf0de0132</uuid>
  <locale>en_US</locale>
  <definition>
<DT xmlns="http://schemas.ilog.com/Rules/7.0/DecisionTable" Version="7.0">
  <Body>
    <Preconditions>
      <Text><![CDATA[definitions]]>&#13;<![CDATA[
    set 'the transaction' to a business entity in the entities of 'the validation request']]>&#13;<![CDATA[
            where the name of this business entity is "transaction" ;]]>&#13;<![CDATA[
    set 'product variant' to an attribute in the attributes of 'the transaction']]>&#13;<![CDATA[
            where the name of this attribute is "transaction"]]>&#13;<![CDATA[
            and the entity name of this attribute is "productVariant" ;]]>&#13;<![CDATA[
   set 'the name' to a business entity in the entities of 'the validation request']]>&#13;<![CDATA[
            where the name of this business entity is "name" ;]]>&#13;<![CDATA[
    set 'receiver name attribute' to an attribute in the attributes of 'the name';]]>&#13;<![CDATA[
        ]]></Text>
    </Preconditions>
    <Structure>
      <ConditionDefinitions>
        <ConditionDefinition Id="C0">
          <ExpressionDefinition>
            <Text><![CDATA[the value of 'product variant' is one of <strings>]]></Text>
          </ExpressionDefinition>
        </ConditionDefinition>
        <ConditionDefinition Id="C1">
          <ExpressionDefinition>
            <Text><![CDATA[the name of 'receiver name attribute' is <a string>]]></Text>
          </ExpressionDefinition>
        </ConditionDefinition>
        <ConditionDefinition Id="C2">
          <ExpressionDefinition>
            <Text><![CDATA[the entity name of 'receiver name attribute'  is <a string>]]></Text>
          </ExpressionDefinition>
        </ConditionDefinition>
        <ConditionDefinition Id="C3">
          <ExpressionDefinition>
            <Text><![CDATA[the value of 'receiver name attribute' is <a format>]]></Text>
          </ExpressionDefinition>
        </ConditionDefinition>
      </ConditionDefinitions>
      <ActionDefinitions>
        <ActionDefinition Id="A0">
          <ExpressionDefinition>
            <Text><![CDATA[set validation error for the entity name of 'receiver name attribute'  with rule id <a string> , rule name <a string> , rule reference id <a string> , error code <a string> , error message <a string> , persistence flag <a boolean> and verifyField flag <a boolean> to 'the validation response']]></Text>
          </ExpressionDefinition>
        </ActionDefinition>
      </ActionDefinitions>
    </Structure>
    <Contents>
      <Partition DefId="C0">
        <Condition>
          <Expression>
            <Param><![CDATA[{ "UBP" , "PREPAY" }]]></Param>
          </Expression>
          <Partition DefId="C1">
            <Condition>
              <Expression>
                <Param><![CDATA["firstName"]]></Param>
              </Expression>
              <Partition DefId="C2">
                <Condition>
                  <Expression>
                    <Param><![CDATA["receiverFirstName"]]></Param>
                  </Expression>
                  <Partition DefId="C3">
                    <Condition>
                      <Expression>
                        <Param><![CDATA[NOT EMPTY]]></Param>
                      </Expression>
                      <ActionSet>
                        <Action DefId="A0">
                          <Expression>
                            <Param><![CDATA["UBPPREPAY_RFMLNNEC"]]></Param>
                            <Param><![CDATA["UBP PREPAY Receiver First  Middle and Last Name Non Empty Check"]]></Param>
                            <Param><![CDATA["5285"]]></Param>
                            <Param><![CDATA["126"]]></Param>
                            <Param><![CDATA["Receiver name not allowed."]]></Param>
                            <Param><![CDATA[false]]></Param>
                            <Param><![CDATA[false]]></Param>
                          </Expression>
                        </Action>
                      </ActionSet>
                    </Condition>
                  </Partition>
                </Condition>
              </Partition>
            </Condition>
            <Condition>
              <Expression>
                <Param><![CDATA["middleName"]]></Param>
              </Expression>
              <Partition DefId="C2">
                <Condition>
                  <Expression>
                    <Param><![CDATA["receiverMiddleName"]]></Param>
                  </Expression>
                  <Partition DefId="C3">
                    <Condition>
                      <Expression>
                        <Param><![CDATA[NOT EMPTY]]></Param>
                      </Expression>
                      <ActionSet>
                        <Action DefId="A0">
                          <Expression>
                            <Param><![CDATA["UBPPREPAY_RFMLNNEC"]]></Param>
                            <Param><![CDATA["UBP PREPAY Receiver First  Middle and Last Name Non Empty Check"]]></Param>
                            <Param><![CDATA["5285"]]></Param>
                            <Param><![CDATA["126"]]></Param>
                            <Param><![CDATA["Receiver name not allowed."]]></Param>
                            <Param><![CDATA[false]]></Param>
                            <Param><![CDATA[false]]></Param>
                          </Expression>
                        </Action>
                      </ActionSet>
                    </Condition>
                  </Partition>
                </Condition>
              </Partition>
            </Condition>
            <Condition>
              <Expression>
                <Param><![CDATA["lastName"]]></Param>
              </Expression>
              <Partition DefId="C2">
                <Condition>
                  <Expression>
                    <Param><![CDATA["receiverLastName"]]></Param>
                  </Expression>
                  <Partition DefId="C3">
                    <Condition>
                      <Expression>
                        <Param><![CDATA[NOT EMPTY]]></Param>
                      </Expression>
                      <ActionSet>
                        <Action DefId="A0">
                          <Expression>
                            <Param><![CDATA["UBPPREPAY_RFMLNNEC"]]></Param>
                            <Param><![CDATA["UBP PREPAY Receiver First  Middle and Last Name Non Empty Check"]]></Param>
                            <Param><![CDATA["5285"]]></Param>
                            <Param><![CDATA["126"]]></Param>
                            <Param><![CDATA["Receiver name not allowed."]]></Param>
                            <Param><![CDATA[false]]></Param>
                            <Param><![CDATA[false]]></Param>
                          </Expression>
                        </Action>
                      </ActionSet>
                    </Condition>
                  </Partition>
                </Condition>
              </Partition>
            </Condition>
          </Partition>
        </Condition>
      </Partition>
    </Contents>
  </Body>
  <Resources DefaultLocale="en_US">
    <ResourceSet Locale="en_US">
      <Data Name="Definitions(A0)#HeaderText"><![CDATA[Set Validation Error]]></Data>
      <Data Name="Definitions(A0)#Width"><![CDATA[423]]></Data>
      <Data Name="Definitions(A0)[0]#HeaderText"><![CDATA[Rule ID]]></Data>
      <Data Name="Definitions(A0)[1]#HeaderText"><![CDATA[Rule Name]]></Data>
      <Data Name="Definitions(A0)[2]#HeaderText"><![CDATA[Rule Ref ID]]></Data>
      <Data Name="Definitions(A0)[3]#HeaderText"><![CDATA[Error Code]]></Data>
      <Data Name="Definitions(A0)[4]#HeaderText"><![CDATA[Error Message]]></Data>
      <Data Name="Definitions(A0)[5]#HeaderText"><![CDATA[Persistance Flag]]></Data>
      <Data Name="Definitions(A0)[6]#HeaderText"><![CDATA[VerifyField Flag]]></Data>
      <Data Name="Definitions(C0)#HeaderText"><![CDATA[Product Variant]]></Data>
      <Data Name="Definitions(C0)#Width"><![CDATA[362]]></Data>
      <Data Name="Definitions(C1)#HeaderText"><![CDATA[Name]]></Data>
      <Data Name="Definitions(C1)#Width"><![CDATA[361]]></Data>
      <Data Name="Definitions(C2)#HeaderText"><![CDATA[Entity Name]]></Data>
      <Data Name="Definitions(C2)#Width"><![CDATA[362]]></Data>
      <Data Name="Definitions(C3)#HeaderText"><![CDATA[Value of Name attribute is]]></Data>
      <Data Name="Definitions(C3)#Width"><![CDATA[297]]></Data>
    </ResourceSet>
  </Resources>
</DT></definition>
  <effectiveDate>2016-07-01T00:00:00.000-0500</effectiveDate>
  <expirationDate>2099-12-31T00:00:00.000-0600</expirationDate>
</ilog.rules.studio.model.dt:DecisionTable>
